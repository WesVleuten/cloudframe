// const assert = require('assert').strict;
let testqueue = [];
let doing = false;

const reset = '\x1b[0m';
const green = msg => '\x1b[32m' + msg + reset;
const red = msg => '\x1b[31m' + msg + reset;

const stats = {
    tests: 0,
    asserts: 0,
    success: 0,
};

const display = (msg, success=true) => {
    stats.asserts++;
    if (success) {
        stats.success++;
    }
    console.log('\t' + (success? green('\u2713'): red('X')) + ' | ' + msg);
}

const assert = {
    ok: (x=true, msg='OK') => {
        display(msg, Boolean(x));
    },
    fail: (err='FAIL') => {
        display(err, false);
    },

    equal: (a, b, msg='Equality') => {
        display(msg, a == b);
    },
    strictEqual: (a, b, msg='StrictEquality') => {
        display(msg, a === b);
    },
    deepEqual: (a, b, msg='DeepEqual') => {
        display(msg, JSON.stringify(a) === JSON.stringify(b));
    },

    notEqual: (a, b, msg='Equality') => {
        display(msg, a != b);
    },
    notStrictEqual: (a, b, msg='StrictEquality') => {
        display(msg, a !== b);
    },
    notDeepEqual: (a, b, msg='DeepEqual') => {
        display(msg, JSON.stringify(a) !== JSON.stringify(b));
    },

    greaterThan: (a, b, msg='GreaterThan') => {
        display(msg, a > b);
    },
    lesserThan: (a, b, msg='LesserThan') => {
        display(msg, a < b);
    },
};

module.exports = function(msg, func) {

    testqueue.push({
        message: msg,
        func: func
    });

    if (doing) return;
    doing = true;
    doQueue();
};

const doQueue = function() {
    if (testqueue.length == 0) {
        doing = false;
        doneTest();
        return;
    }

    clearDoneTest();
    const currentTest = testqueue.shift();

    console.log(' - ' + currentTest.message);
    
    if (typeof currentTest.func != 'function') {
        assert.fail('Not a function');
        doQueue();
        return;
    }

    currentTest.func(assert, function() {
        stats.tests++;
        doQueue();
    });
};

let testTimeout = null;
const clearDoneTest = function() {
    clearTimeout(testTimeout);
    testTimeout = null;
};
const doneTest = function() {
    clearDoneTest();
    testTimeout = setTimeout(handleDoneTest, 200);
};
const handleDoneTest = function() {
    console.log('-------');
    console.log('Tested ' + stats.tests + ' units');
    console.log('With ' + stats.asserts + ' assertions');
    const fails = stats.asserts - stats.success;
    console.log('Of those ' + (fails > 0? red(fails) : green(fails)) + ' failed');
    process.exit();
};