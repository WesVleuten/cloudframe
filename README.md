# CloudFrame

Welcome to cloudframe, it tries to be an easy to use framework to get you going quickly. Below you'll find the documentation on how to use it.

## Table of contents

- [Install](#install)
- [Starting](#starting)
- [Config](#config)
- [Initialization](#initialization)
- [Controller](#controller)
- [Middleware](#middleware)
- [Logic](#Logic)
- [Repository](#Repository)
- [Helpers](#helpers)
- [Database](#database)
- [Wrapper](#wrapper)
- [Session](#session)
- [Legacy_Mode](#Legacy_Mode)

---

## Install

To install this framework in your project open your terminal and navigate to your project and enter:
```cmd
npm install cloudframe -s
```

[back to top](#table-of-contents)

---

## Starting

Starting the project is very easy just enter the following command:

Linux/macOS
```cmd
DEBUG=* node index.js
```
Windows command line
```cmd
set DEBUG=* && node index.js
```
Windows power shell
```cmd
$env:DEBUG="*"; node index.js
```

The debug enviroment variable indicated to the project that you want to log all messages to your console window. The npm module log is used for this. 

For the production eviroment you should use a different command. This will indicate to your application to use the production config file.
```cmd
NODE_ENV=production DEBUG=* node index.js
```

It is recommended to put these commands in the package.json in your project for ease of use.

[back to top](#table-of-contents)

---

## Config

By default we use config.json and config.prod.json (depending on eviroment) in the root of the application, this file should have the following properties:

| Property            | Description                                                               | Default  |
| ------------------- | ------------------------------------------------------------------------- | -------- |
| `session`           | Boolean indicating if you want the framework to handle sessions           | REQUIRED |
| `port`              | Number, port the http server will listen on                               | REQUIRED |
| `database.host`     | String, hostname/ip where mysql database is located                       | REQUIRED |
| `database.user`     | String, username which will be used to create an connection               | REQUIRED |
| `database.password` | String, password which will be used to create an connection               | REQUIRED |
| `database.database` | String, database that will be connected to                                | REQUIRED |
| `socket`            | Boolean indicating if you want the framework to handle socket connections | false    |

[back to top](#table-of-contents)

---

## Initialization

Initializing is quick and easy.

```js
require('cloudframe')([options]);
```

NOTE: the square bracket indicate that the options parameter is optional, this is the case with all further documentation

The options parameter could be an object with the following properties:

| Property     | Description                                                   | Default                                         |
| ------------ | ------------------------------------------------------------- | ----------------------------------------------- |
| `basepath`   | Directory where your project is located                       | Directory from file where it was required from  |
| `apppath`    | Location of your application, these files will be auto-loaded | basepath/app                                    |
| `modelpath`  | Location of your models                                       | basepath/model                                  |
| `middleware` | Default middleware for all controllers                        | crosfix, bodyparse, wrapper, database, session  |

[back to top](#table-of-contents)

---

## Application

[back to top](#table-of-contents)

---

## Controller

#### Controller.get(url, [middleware], callback)

Alias for `Controller.create('get', url, middleware, callback)`

#### Controller.post(url, [middleware], callback)

Alias for `Controller.create('post', url, middleware, callback)`

#### Controller.delete(url, [middleware], callback)

Alias for `Controller.create('delete', url, middleware, callback)`

#### Controller.create(method, url, [middleware], callback)

It is recommended to use the aliasses instead of directly this function.

| Parameter    | Description                                                    | Default     |
| ------------ | -------------------------------------------------------------- | ----------- |
| `method`     | HTTP method                                                    | REQUIRED    |
| `url`        | Exact route for listening to                                   | REQUIRED    |
| `middleware` | Array of middleware identifiers additional to the default ones | Empty array |
| `callback`   | Function that will be run if url is called                     | REQUIRED    |

The callback function will be given two parameters, a request object and a responds object. Usually these are identified by `req` and `res` respsectivly. The request object contains all information incoming from the client and from the middlewares, and the response object contains all outgoing information which will be send to the client.

The `middleware` property is always in order of execution, starting at index 0.

For complete documentation of the request and response object you should reference the [express.js documentation](https://expressjs.com/en/4x/api.html#req), since the objects provided are those but extended by the default middleware.

**If** the request has a body (usually with `POST` requests) the body is accesible with `req.body`. It will be parsed according to the type of input.
However file uploads are not support by the middleware used for this. For file uploads you'll need to add your own middleware to handle those types of requests.

#### Controller.registry

The registry property is an array containing all routes currently configured

[back to top](#table-of-contents)

---

## Middleware

#### Controller.createMiddleware(identifier, callback)

This function adds new middleware under an identifier to the list of accessible middleware.

Example:
```js
Controller.createMiddleware('simplelog', function(req, res, next) {
    console.log(req.method, req.url, req.path);
    next();
});

//how to use it in an controller
Controller.get('/test', ['simplelog'], function(req, res) {
    // simplelog will be ran before this function
    res.sendWrapped(null, {
        message: 'Hello world'
    });
});
```

`req` and `res` are both the same as in the controller, but please note in what order middleware is executed for what exact properties are available. 

[back to top](#table-of-contents)

---

## Logic

### Logic.register([name], func);

Register a new logic function. Name will be set to function name if omitted.

### Logic.execute([name], args...);

Execute an logic function with optional arguments. This will return whatever the registered function returns. In best practice cases this should be an Promise object.

### Example

```js
// logic file
module.exports = function({ Logic, Repository }) {
    Logic.register(async function getUsersByGroup(groupName) {
        return new Promise((resolve, reject) => {
            //get group
            const group = await Repository.execute('getGroup', groupName);
            //get userlist
            let user = await Repository.execute('getUserList');

            //filter userlist and return
            return user.filter(x => x.group == group.id);
        });
    });
};

// other file with access to Logic object:
const groupName = 'admin';
Logic.execute('getUserList', groupName);
```

[back to top](#table-of-contents)

---

## Repository

### Repository.register([name], func);

Register a new repository. Name will be set to function name if omitted.

### Repository.execute([name], args...);

Execute an repository with optional arguments. This will return whatever the registered function returns. In best practice cases this should be an Promise object.

### Example

```js
// repository file
module.exports = function({ Repository, Database }) {
    Repository.register(function getUserList() {
        // database.query returns a promise, no need to wrap
        return Database.query(`SELECT * FROM User`);
    });
};
```

[back to top](#table-of-contents)

---

## Helpers

### Creating a helper

```js
Helper.create(name, callback);
```

Using this will add the callback under the key `name` in the Helper object.

### Using a helper

```js
Helper.name([parameters]);
```

[back to top](#table-of-contents)


---

## Database

### Non strict

The database can be accessed using `req.database`. It will be a connected instance of the mysql npm module. That documentation can be found [here](https://github.com/mysqljs/mysql/blob/master/Readme.md).

### Strict and Repositories

#### Database.query(sqlquery, values)

Runs a query (`sqlquery`) with injected `values` on the database. Returns a promise.

[back to top](#table-of-contents)

---

## Wrapper

There are multiple types of responses the wrapper can deliver, the names are very descriptive.

```js
res.sendWrapped(err, data); //send 200 OK with attached data. But send server error if err is filled.
res.sendServerError(err); //sends 500 server error and logs error to database.
res.sendNotFound(); //sends 404 Not Found
res.sendForbidden(); //sends 403 Forbidden
res.sendBadRequest(); //sends 400 Bad Request
res.sendNoContent(); //sends 204 No Content
res.sendError(err); // sendServerError alias
res.sendErr(err); // sendServerError alias
res.sendData(err, data); // sendWrapped alias
```

[back to top](#table-of-contents)

---

## Session

The session can be accessed using `req.session` **if** an authorization header/query/cookie has been set.

If no authorization token has been given, or the session can't be found, the property will be `null`

The object looks as follows:

| Property  | Description                                |
| --------- | ------------------------------------------ |
| `id`      | Session id                                 |
| `user`    | User id                                    |
| `key`     | Token for authorzation                     |
| `created` | Date object for when the token was created |

[back to top](#table-of-contents)

---

## Legacy_Mode

To support the applications build on the api of version 1, the legacy mode is availible. When legacy mode is active the new api is also availible. But note that strictness is gone.

In legacy mode the old `Model` object is available.

Enabeling legacy mode can be done in your index file. It should be passed through the options.
```js
require('cloudframe')({
    legacy: true
});
```

[back to top](#table-of-contents)

---

## Strict_mode

Strict mode provides a strict way of seperation of concerns. This forces you to build within a structure.

With strict mode files in the controller folder only have access to the `Controller` and `Logic` objects, Logic files only have access to `Logic` and `Repository`, and Repository has access to `Repository` and `Database`.

This is turned off by default. But highly recommended to turn on.

```js
require('cloudframe')({
    strict: true
});
```

[back to top](#table-of-contents)