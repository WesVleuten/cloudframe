module.exports = function({ Controller, Model }) {
    Controller.createMiddleware('database', function DatabaseMiddleware(req, res, next) {
        req.database = Model.Database;
        next();
    }, 'defaults');
};