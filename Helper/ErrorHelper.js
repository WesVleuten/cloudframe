module.exports = async (file, insert) => {
	const fs = require('fs');
	const LOG_DIR = './log';

	return new Promise(async (resolve, reject) => {
		const now = new Date();
		const zdate = x => x < 10 ? '0' + x : x;
		const zmill = x => x < 100 ? (x < 10 ? '00' + x : '0' + x) : x;
		const timestring = now.getUTCFullYear() + '-' + zdate(now.getUTCMonth() +1) + '-' + zdate(now.getUTCDate()) + 'T' + zdate(now.getUTCHours()) + ':' + zdate(now.getUTCMinutes()) + ':' + zdate(now.getUTCSeconds()) + '.' + zmill(now.getUTCMilliseconds());
		fs.stat(LOG_DIR, async (err, stat) => {
			if (err && err.code != 'ENOENT') {
				reject(err);
				return;
			}
			if ((err && err.code == 'ENOENT') || !stat.isDirectory()) {
				await (new Promise((resolve, reject) => {
					fs.mkdir(LOG_DIR, (err, stat) => {
						if (err) {
							reject(err);
							return;
						}
						resolve();
					});
				}));
			}
			fs.appendFile(LOG_DIR + '/' + file, timestring + ' - ' + JSON.stringify(insert) + '\n', err => {
				if (err) {
					reject(err);
					return;
				}
				resolve();
			});
		});

	});
};