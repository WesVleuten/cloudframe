const debug = require('debug')('cf:main');

process.stdin.resume();
process.on('exit', function () {
    debug('Shutting down..');
    process.exit(0);
});

process.on('SIGINT', function () {
    debug('Ctrl-C...');
    process.exit(1);
});