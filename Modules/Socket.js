var socketlog = require('debug')('cf:socket');
var WebSocket = require('ws');
var EventEmitter = require('events');

var sessionCollection = [];

const getSocketByUserId = function(userid, cb) {
    let i = 0;
    const iMax = sessionCollection.length;
    let result = [];
    for(; i < iMax; i++) {
        if (sessionCollection[i].session.user == userid || sessionCollection[i].session.account == userid)
            result.push(sessionCollection[i].socket);
    }
    cb(null, result);
};

module.exports = {
    init: function(db, server) {
        const wss = new WebSocket.Server({
            server: server,
            verifyClient: function(info, cb) {
                const url = require('url');
                const urlp = url.parse(info.req.url, true);
                let token = null;
                if (urlp.query.auth) {
                    token = urlp.query.auth;
                }
                if (urlp.query.token) {
                    token = urlp.query.token;
                }
                if (token === null) {
                    cb(false, 400, 'Bad request');
                    return;
                }
                let SessionHelper = require('../Helper/SessionHelper.js');
                SessionHelper.checkKey(db, token, (err, session) => {
                    if (err == 404) {
                        cb(false, 401, 'Unauthorized');
                        return;
                    }
                    if (err) {
                        cb(false, 500, 'Internal server error');
                        return;
                    }
                    info.req.session = session;
                    cb(true);
                });
            }
        });
        
        wss.on('connection', function connection(ws, basereq) {
            const session = basereq.session;
            const sessionCol = {
                session: session,
                socket: ws
            };
            sessionCollection.push(sessionCol);
            const sendBase = function(rid) {
                return {
                    sendNotFound: function() {
                        this.sendRawData("error", 404, "notfound");
                        return res.status(404).send('Not Found');
                    },
                    sendForbidden: function() {
                        this.sendRawData("error", 403, "forbidden");
                        return res.status(403).send('Forbidden');
                    },
                    sendNoContent: function(err) {
                        this.sendRawData("error", 204, "nocontent");
                        return res.status(204).end();
                    },
                    sendWrapped: function(err, d) {
                        if (err) return this.sendServerError(err);
                        this.sendRawData("result", 200, d);
                    },
                    sendBadRequest: function() {
                        this.sendRawData("error", 400, "badrequest");
                    },
                    sendServerError: function(e) {
                        this.sendRawData("error", 500, e);
                    },
                    sendRawData: function(returntype, code, data) {
                        ws.send(JSON.stringify({
                            "success": code == 200,
                            "type": returntype,
                            "code": code,
                            "data": data,
                            "rid": rid
                        }));
                    }
                };
            };
            socketlog('connected to a client');
        
            ws.addEventListener('close', function(code, message) {
                const index = sessionCollection.indexOf(sessionCol);
                if (index > -1) {
                    sessionCollection.splice(index, 1);
                }
                socketlog('disconnected a client');
            });
            
            ws.on('error', function(e) {
                socketlog('connection err', e);
            });
        });
    },
    sendNotification: function(userid, message, cb) {
        getSocketByUserId(userid, function(err, clients) {
            if (err) {
                if (typeof cb == 'function') cb(err);
                return;
            }
            let i = 0;
            const iMax = clients.length;
            let result = [];
            for(; i < iMax; i++) {
                clients[i].send(JSON.stringify({
                    "type": "notification",
                    "code": 200,
                    "data": {
                        "message": message
                    }
                }));
            }
            if (typeof cb == 'function') cb();
        });
    },
    sendData: function(userid, data, cb) {
        getSocketByUserId(userid, function(err, clients) {
            if (err) {
                if (typeof cb == 'function') cb(err);
                return;
            }
            let i = 0;
            const iMax = clients.length;
            let result = [];
            for(; i < iMax; i++) {
                clients[i].send(JSON.stringify({
                    "type": "rawdata",
                    "code": 200,
                    "data": data
                }));
            }
            if (typeof cb == 'function') cb();
        });
    },
    broadcast: function(data) {
        wss.clients.forEach(function each(client) {
            if (client.readyState !== WebSocket.OPEN) return;
            client.send(JSON.stringify({
                "type": "rawdata",
                "code": 200,
                "data": data
            }));
        });
    }
};
