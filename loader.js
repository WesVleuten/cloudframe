var fs = require('fs');
var loader = function(basepath, fileexec, cb) {
	fs.stat(basepath, function(err, stat) {
		if (err) return cb(err);
		if (!stat.isDirectory()) return cb();
		fs.readdir(basepath, function(err, list) {
			if (err) return cb(err);
			if (list.length == 0) return cb();
			async.each(list, function(item, ecb) {
				var path = basepath + '/' + item;
				if (item.length > 1 && item[0] == '.') return ecb();
				fs.stat(path, function(err, stat) {
					if (err) return ecb(err);
					if (stat.isFile()) {
						debug('loading file:', path);
						fileexec(path, ecb);
						return;
					}
					if (stat.isDirectory()) {
						debug('loading directory:', path);
						loader(path, fileexec, ecb);
						return;
					}
					ecb();
				});
			}, cb);
		});
	});
};

var debug = require('debug')('cf:loader');
var async = require('async');
module.exports = exports = function(config, scb) {
	var path = config.path;
	fs.stat(path, function(err, stat) {
		if (err) {
			console.log(err);
			if (err.code == 'ENOENT') {
				scb();
			} else {
				scb(err);
			}
			return;
		}
		if (!stat.isDirectory()) {
			scb();
			return;
		}
		
		loader(path, function(filepath, cb) {
			const file = require(filepath);
			if (typeof file == 'function') {
				file(config.args);
			}
			debug('file: ' + filepath + ' has been loaded');
			cb();
		}, err => scb(err));
	});
};
