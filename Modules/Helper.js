module.exports = {
    add: function(name, func) {
        if (typeof name == 'function') {
            func = name;
            name = func.name;
        }
        if (this[name] !== undefined) {
            throw new Error('Cannot create a helper with that name, name is in use');
        }
        this[name] = func;
        return true;
    }
};