let exceptionThrown = false;
const log = require('debug')('exception');
const warn = require('debug')('warning');
const logFile = require('../Helper/ErrorHelper');
process.on('uncaughtException', async (exception) => {
	log(exception);
	if (exceptionThrown) {
    	process.exit(1);
	}
	exceptionThrown = true;
	try {
		await logFile('exception.log', {
			message: exception.message,
			stacktrace: exception,
		});
		process.exit(1);
	} catch (exceptioninception) {
		log('exception inception #123#123??');
		log(exception);
		log(exceptioninception);
    	process.exit(1);
	}
});
process.on('unhandledRejection', (reason, p) => {
	const msg = 'Unhandled Rejection at: ' + p + ' reason: ' + reason;
	warn(msg);
	logFile('rejection.log', msg).catch(console.log);
});
process.on('warning', w => {
	warn(w);
	logFile('warning.log', w).catch(console.log);
});
  