module.exports = function(opt={}) {
	require('./Handler/ExceptionHandler.js');
	require('./Handler/ShutdownHandler.js');
	
	const path = require('path');
	const debug = require('debug');
	const mainlog = debug('cf:main');
	const express = require('express');
	const async = require('async');
	const app = express();
	
	if (!opt.basepath) {
		opt.basepath = path.dirname(require.main.filename);
	}
	
	if (!opt.apppath) {
		opt.apppath = opt.basepath + '/app';
	}

	if (!opt.modelpath) {
		opt.modelpath = opt.apppath + '/model';
	}

	if (!opt.repositorypath) {
		opt.repositorypath = opt.apppath + '/repository';
	}

	if (!opt.logicpath) {
		opt.logicpath = opt.apppath + '/logic';
	}

	if (!opt.controllerpath) {
		opt.controllerpath = opt.apppath + '/controller';
	}

	if (!opt.helperpath) {
		opt.helperpath = opt.apppath + '/helper';
	}

	if (!opt.middlewarepath) {
		opt.middlewarepath = opt.apppath + '/middleware';
	}

	if (!opt.middleware) {
		opt.middleware = ['crosfix', 'bodyparse', 'wrapper', 'database', 'session'];
	}
	
	if (!opt.config) {
		if (process.env && process.env.NODE_ENV && (process.env.NODE_ENV == 'unittest' || process.env.NODE_ENV == 'production')) {
			if (process.env.NODE_ENV == 'production') {
				opt.config = opt.basepath + '/config.prod.json';
			} else if (process.env.NODE_ENV == 'unittest') {
				opt.config = opt.basepath + '/config.test.json';
			}
		} else {
			opt.config = opt.basepath + '/config.json';
		}
	}

	if (opt.legacy === undefined) {
		opt.legacy = false;
	}
	if (opt.strict === undefined) {
		opt.strict = false;
	}
	
	app.disable('x-powered-by');

	mainlog('loading API');
	mainlog('basepath: ' + opt.basepath);
	mainlog('apppath: ' + opt.apppath);
	mainlog('modelpath: ' + opt.modelpath);
	mainlog('middleware: ' + opt.middleware);
	mainlog('configfile: ' + opt.config);
	mainlog('legacy mode: ' + opt.legacy);
	mainlog('strict mode: ' + opt.strict);

	const Controller = require('./Modules/Controller');
	const Logic = require('./Modules/Logic');
	const Model = require('./Modules/Model');
	const Socket = require('./Modules/Socket');
	const Helper = require('./Modules/Helper');
	const Database = require('./Modules/Database');
	const Repository = require('./Modules/Repository');
	let Config = null;

	const compeleteArg = {
		Controller,
		Logic,
		Model,
		Socket,
		Helper,
		Database,
		Repository
	};

	async.waterfall([
		cb => {
			const fs = require('fs');
			fs.readFile(opt.config, 'utf8', function(err, data) {
				if (err) {
					cb(err);
					return;
				}
				Config = JSON.parse(data);
				if (opt.legacy) {
					global.Config = Config;
					global.Controller = Controller;
					global.Model = Model;
					global.Socket = Socket;
					global.Helper = Helper;
				}
				mainlog('config loaded');
				cb();
			});
		},
		cb => {
			Controller.setApplication(app);
			cb();
		},
		cb => {
			if (!Config.database) return cb();
			Database.connect(Config.database).then(() => cb()).catch(err => cb(err));
		},
		cb => {
			if (!Config.database) return cb();
			Model.connect(Database, err => cb(err) );
		},
		cb => {
			require('./Modules/Application.js');
			cb();
		},
		cb => {
			const args = {
				Controller,
				Database,
				Model,
			};
			if (opt.middleware.indexOf('crosfix') != -1) {
				require('./Middleware/CROSMiddleware')(args);
			}
			if (opt.middleware.indexOf('bodyparse') != -1) {
				require('./Middleware/BodyParseMiddleware')(args);
			}
			if (opt.middleware.indexOf('wrapper') != -1) {
				require('./Middleware/ResponseMiddleware')(args);
			}
			if (opt.middleware.indexOf('database') != -1 && opt.strict === false) {
				require('./Middleware/DatabaseMiddleware')(args);
			}
			if (opt.middleware.indexOf('session') != -1 && Config.session === true) {
				require('./Middleware/SessionMiddleware')(args);
			}

			if (typeof opt.customMiddlewareSetup == 'function') {
				opt.customMiddlewareSetup(app, cb);
			} else {
				cb();
			}
		},
		cb => {
			const loader = require('./loader');
			if (opt.legacy) {
				loader({
					path: opt.apppath,
					args: compeleteArg,
				}, cb);
				return;
			}
			async.series([
				scb => {
					loader({
						path: opt.middlewarepath,
						args: {
							Controller,
							Logic,
						}
					}, scb);
				},
				scb => {
					loader({
						path: opt.controllerpath,
						args: {
							Controller,
							Logic,
						}
					}, scb);
				},
				scb => {
					loader({
						path: opt.logicpath,
						args: {
							Logic,
							Helper,
							Socket,
							Repository,
						}
					}, scb);
				},
				scb => {
					loader({
						path: opt.repositorypath,
						args: {
							Repository,
							Database,
						}
					}, scb);
				},
				scb => {
					loader({
						path: opt.helperpath,
						args: {
							Helper,
							Repository,
						}
					}, scb);
				}
			], err => cb(err));
		},
		cb => {
			if (process.env && process.env.NODE_ENV && process.env.NODE_ENV == 'unittest') {
				const loader = require('./loader');

				loader({
					path: opt.basepath + '/test',
					args: Object.assign({ Test: require('./Modules/Test') }, compeleteArg),
				}, cb);
				return;
			}
			const http = require('http');
			let server = http.createServer(app);
			if (Config.socket) {
				Socket.init(Database, server);
			}
			server.listen(Config.port, err => cb(err));
			debug('cf:http')('server started listening on *:' + Config.port);
		}
	], function(err) {
		if (err) {
			console.log(err);
			debug('cf:error')(err);
		}
	});
};