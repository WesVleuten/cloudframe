module.exports = function({ Controller }) {
    const bodyParser = require('body-parser');
    Controller.createMiddleware('bodyparse-json', bodyParser.json(), 'defaults');
    Controller.createMiddleware('bodyparse-urlenc', bodyParser.urlencoded({ extended: true }), 'defaults');
    Controller.createMiddleware('fileupload', bodyParser.raw({ type: 'application/octet-stream', limit: '50mb' }));
};