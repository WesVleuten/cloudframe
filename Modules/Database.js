/**
 * Database
 * @module Database
 */

const mysql = require('mysql');
let _connection = null;

/**
 * Database object
 * @name Database
 * @global
 */
module.exports = {
    /**
    * Create a database connection
    * @name Controller.connect
    * @function
    * @param {Object|string} databaseconfig Object that contains database config or a connection string
    * @returns {Promise} Connection handler
    */
    connect: function connect(databaseconfig) {
        return new Promise((resolve, reject) => {
            _connection = mysql.createPool(databaseconfig);
            resolve();
        });
    },

    /**
    * Execute a safe query
    * @name Controller.query
    * @function
    * @param {string} querystring Query to be executed
    * @param {Array} values Values to be injected into the querys
    * @param {Function} callback Function callback carrining result
    * @returns {Promise} Result
    */
    query: function query(querystring, values, callback) {
        return new Promise((promiseResolve, promiseReject) => {
            const resolve = result => {
                promiseResolve(result);
                if (typeof callback == 'function') {
                    callback(null, result);
                }
            };
            const reject = err => {
                promiseReject(err);
                if (typeof callback == 'function') {
                    callback(err);
                }
            };

            _connection.getConnection((err, connection) => {
                if (err) {
                    reject(err);
                    return;
                }
    
                connection.query(querystring, values, (err, result) => {
                    if (err) {
                        reject(err);
                        return;
                    }
    
                    connection.release();
                    resolve(result);
                });
            });
        });
    },

    /**
    * End database connection
    * @name Controller.end
    * @function
    * @returns {Promise} Shows if disconnect was succesful
    */
    end: function end() {
        return new Promise((resolve, reject) => {
            _connection.end(err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }
};