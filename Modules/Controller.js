/**
 * Controller
 * @module Controller
 */

/**
 * Object for Controller creation
 * @name Controller
 * @global
 */
module.exports = {
    registry: [],
    app: null,

    /**
     * Create a get route
     * @name Controller.get
     * @function
     * @param {string} url Path for controller
     * @param {Array.<string|Function>} middleware Array of middleware identifiers to be loaded
     * @param {Controller.RequestHandler} func Function to be run after middleware
     */
    get: function(url, middleware, func) {
        this.create('get', url, middleware, func);
    },
    
    /**
     * Create a post route
     * @name Controller.post
     * @function
     * @param {string} url Path for controller
     * @param {Array.<string|Function>} middleware Array of middleware identifiers to be loaded
     * @param {Controller.RequestHandler} func Function to be run after middleware
     */
    post: function(url, middleware, func) {
        this.create('post', url, middleware, func);
    },

    /**
     * Create a delete route
     * @name Controller.delete
     * @function
     * @param {string} url Path for controller
     * @param {Array.<string|Function>} middleware Array of middleware identifiers to be loaded
     * @param {Controller.RequestHandler} func Function to be run after middleware
     */
    delete: function(url, middleware, func) {
        this.create('delete', url, middleware, func);
    },

    /**
     * Create a route
     * @function
     * @param {string} method REST method for controller
     * @param {string} url Path for controller
     * @param {Array.<string|Function>} middleware Array of middleware identifiers to be loaded
     * @param {Controller.RequestHandler} func Function to be run after middleware
     */
    create: function(method, url, middleware, func) {
        let params = [ url ];
        if (middleware) {
            if (typeof middleware == 'function') {
                func = middleware;
            } else if (Array.isArray(middleware)) {
                for (let i = 0; i < middleware.length; i++) {
                    let name = middleware[i];
                    if (typeof name == 'function') {
                        params.push(name);
                        continue;
                    }
                    if (!this.middleware[name]) {
                        throw Error('middleware "' + name + '" not found');
                    }
                    params.push(this.middleware[name]);
                }
            }
        } else {
            func = middleware;
        }

        params.push(func);
        this.registry.push({
            method: method,
            url: url
        });
        this.app[method.toLowerCase()].apply(this.app, params);
    },
    
    /**
     * Registry for middleware, this contains all non-default middleware
     * @name Controller.middleware
     */
    middleware: {},

    /**
     * Create middleware
     * @name Controller.createMiddleware
     * @function
     * @param {string} name Path for controller
     * @param {Controller.MiddlewareHandler} func Array of middleware identifiers to be loaded
     * @param {string} [type] Function to be run after middleware
     */
    createMiddleware: function(name, func, type) {
        if (type && type == 'defaults') {
            this.app.use(func);
            return;
        }
        this.middleware[name] = func;
    },

    /**
     * Set express application to bind to
     * 
     * Note that this should be done initially, routes that have been set will not be set again.
     * @name Controller.setApplication
     * @function
     * @param {Object} app Express application
     */
    setApplication: function(app) {
        this.app = app;
    }
};

/**
 * This callback is displayed as part of the Requester class.
 * @callback Controller.RequestHandler
 * @function
 * @param {Object} req Request object, an extended version of the express request object
 * @param {Object} res Response object, an extended version of the express response object
 */

 /**
 * This callback is displayed as part of the Requester class.
 * @callback Controller.MiddlewareHandler
 * @function
 * @param {Object} req Request object, an extended version of the express request object
 * @param {Object} res Response object, an extended version of the express response object
 * @param {Function} next Function to call after execution 
 */
