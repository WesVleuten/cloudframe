// this module generates modules from app/modules.
// it allows a more generic way of communicating with the external world by standardizing input and output
// DEPRECATED 

const log = require('debug')('cf:model');
const dblog = require('debug')('cf:model:db');
let firstConnect = true;
let db = null;

const getByNamespace = function(basepointer, name) {
    let namespaces = name.split('.');
    let pointer = basepointer;
    for (let i = 0; i < namespaces.length-1; i++) {
        if (!pointer[namespaces[i]]) { pointer[namespaces[i]] = {}; }
        pointer = pointer[namespaces[i]];
    }
    let lastnamespace = namespaces.pop();
    return pointer[lastnamespace];
};

const valueCasting = {
    cast: function(rawtype, value) {
        let typeoptions = rawtype.split(',');
        let type = typeoptions.shift();
        let typecast = this.types[type];
        if (type.indexOf('[]') == type.length - 2) {
            let returner = [];
            if (!Array.isArray(value)) {
                throw new Error("Array specified was not an array");
            }
            for (let i = 0; i < value.length; i++) {
                returner[i] = this.cast(type.replace('[]', ''), value[i]);
            }
            return returner;
        }
        if (typecast) return typecast(value, typeoptions);
        if (typeof value == 'object') {
            let pointer = module.exports;
            let namespacesplit = type.split('.');
            for (let i = 0; i < namespacesplit.length; i++) {
                if (!pointer[namespacesplit[i]]) {
                    throw new Error('Couldnt find `' + type + '` in typecasting');
                }
                pointer = pointer[namespacesplit[i]];
            }
            return new pointer(value);
        }
        throw new Error('Could not cast type "' + type + '"');
    },
    types: {
        "string": function(value) {
            if (value === null) {
                return '';
            }
            if (typeof value == 'string') {
                return value;
            }
            if (typeof value.toString == 'function') {
                return value.toString();
            }
            throw new Error('Could not cast');
        },
        "number": function() {
            return valueCasting.types.integer.apply(this, arguments);
        },
        "int": function() {
            return valueCasting.types.integer.apply(this, arguments);
        },
        "integer": function(value, options) {
            if (value === null) {
                if (options.indexOf('allownull') > -1) {
                    return null;
                }
                return 0;
            }
            if (!value) {
                return 0;
            }
            return parseInt(value);
        },
        "boolean": function(value) {
            if (typeof value == 'string' && (value == 'true' || value == 'false')) {
                return value == 'true';
            }
            return Boolean(value);
        },
        "buffer": function(value) {
            if (value == null) {
                return null;
            }
            return Buffer.from(value, 'utf8');
        },
        "bool": function(value) {
            return valueCasting.types.boolean.apply(this, arguments);
        },
        "datetime": function(value) {
            //handeld by mysql driver
            return value;
        }
    }
};

module.exports = {
    Database: {
        query: function(a,b,c) {
            module.exports.query(a,b,c);
        },
        escape: function(a) {
            return module.exports.escape(a);
        }
    },
    escape: function(a) {
        return require('mysql').escape(a);
    },
    query: function(sql,b,c) {
        var cb = b, values = [];
        if (typeof b != 'function') {
            values = b;
            cb = c;
        }
        if (db == null) return cb('no database connection');

        db.query(sql, values, cb);
    },
    connect: function(inserteddb, cb) {
        if (db !== null) {
            const err = new Error('cant connect twice');
            if (typeof cb == 'function') {
                cb(err);
            } else {
                throw err;
            }
        }
        db = inserteddb;
        cb();
    },
    create: function(name, model) {
        const scope = this;
        if (name == 'create') throw new Error('Model with name create is not allowed');
        log('Model ' + name + ' has been created');
        const databaseContext = {
            tablename: null,
            defaults: [],
            relations: [],
            primaryKey: null,
            deleteProtection: null,
        };
        const methods = {
            constructors: {},
            object: {}
        };
        
        let namespaces = name.split('.');
        let pointer = scope;
        for (let i = 0; i < namespaces.length-1; i++) {
            if (!pointer[namespaces[i]]) { pointer[namespaces[i]] = {}; }
            pointer = pointer[namespaces[i]];
        }
        let lastnamespace = namespaces.pop();
        
        if(pointer[lastnamespace]) {
            throw new Error('Trying to overwrite a model, `' + name + '`');
        }
        
        const modelobject = function(givenparam={}) {
            let obj = Object.assign({}, methods.object);
            
            let modelKeys = Object.keys(model);
            for (let i = 0; i < modelKeys.length; i++) {
                let key = modelKeys[i];
                obj[key] = valueCasting.cast(model[key], (givenparam[key] || null));
            }

            for (let i = 0; i < databaseContext.defaults.length; i++) {
                let def = databaseContext.defaults[i];
                if (obj[def.key] === null) {
                    let val = def.defaultvalue;
                    if (val == 'NOW()' || val == 'CURRENT_TIMESTAMP') {
                        val = new Date();
                    }
                    obj[def.key] = val;
                }
            }

            if (databaseContext.relations.length != 0) {
                databaseContext.relations.forEach(rel => {
                    const relationmethod = cb => {
                        const model = getByNamespace(scope, rel.model);
                        if (rel.externcolumn) {
                            let getter = {};
                            getter[rel.externcolumn] = obj[rel.key];
                            model.get(getter, cb);
                            return;
                        }
                        model.getById(obj[rel.key], cb);
                    };
                    obj['get' + rel.model.replace(/\./g, '')] = relationmethod;
                    obj['get' + rel.model.split('.').pop()] = relationmethod;
                });
            }
            
            obj.save = cb => {
                if (db === null) {
                    throw new Error('No database connection');
                }
                if (databaseContext.tablename === null) {
                    throw new Error('Couldnt save `' + name + '`, no database link');
                }
                if (databaseContext.primaryKey === null) {
                    throw new Error('Couldnt save `' + name + '`, no primaryKey');
                }
                let q = '';
                if (obj[databaseContext.primaryKey] === 0 || obj[databaseContext.primaryKey] === null) {
                    q = db.query("INSERT INTO ?? SET ?", [ databaseContext.tablename, obj ], function(err, result) {
                        if (err) {
                            cb(err);
                            return;
                        }
                        obj[databaseContext.primaryKey] = result.insertId;
                        cb(null, obj);
                    });
                } else {
                    q = db.query("UPDATE ?? SET ? WHERE ?? = ?", [databaseContext.tablename, obj, databaseContext.primaryKey, obj[databaseContext.primaryKey]], function(err, result) {
                        if (err) {
                            cb(err);
                            return;
                        }
                        obj[databaseContext.primaryKey] = result.insertId;
                        cb(null, obj);
                    });
                }
                dblog('query', q.sql.slice(0, 500));
            };
            obj.delete = function(cb) {
                if (db === null) {
                    throw new Error('No database connection');
                }
                if (databaseContext.tablename === null) {
                    throw new Error('Couldnt save `' + name + '`, no database link');
                }
                if (databaseContext.primaryKey === null) {
                    throw new Error('Couldnt save `' + name + '`, no primaryKey');
                }
                if (obj[databaseContext.primaryKey] === null || obj[databaseContext.primaryKey] == 0) {
                    cb('Primarykey is empty');
                    return;
                }

                let q = db.query("DELETE FROM ?? WHERE ?? = ?", [databaseContext.tablename, databaseContext.primaryKey, obj[databaseContext.primaryKey]], function(err, result) {
                    if (err) return cb(err);
                    cb();
                });
                dblog('query', q.sql.slice(0, 500));
            };

            return obj;
        };
        
        modelobject.get = function(search, cb) {

            const parseResult = function(err, result) {
                if (err) return cb(err);
                let returner = [];
                for (let i = 0; i < result.length; i++) {
                    returner.push(new modelobject(result[i]));
                }
                cb(null, returner);
            };

            if (methods.constructors.get) {
                methods.constructors.get(search, parseResult);
                return;
            }

            if (db === null) {
                throw new Error('No database connection');
            }
            if (databaseContext.tablename === null) {
                throw new Error('Couldnt save `' + name + '`, no database link');
            }

            let sqlquery = "SELECT * FROM ?? WHERE ";
            let values = [ databaseContext.tablename ];
            let searchkeys = Object.keys(search);
            for (let i = 0; i < searchkeys.length; i++) {
                const key = searchkeys[i], val = search[key];
                values.push(key);
                if (i != 0) sqlquery += ' &&';
                if (val === null) {
                    sqlquery += ' ?? IS NULL';
                } else {
                    values.push(val);
                    sqlquery += ' ?? = ?';
                }
            }

            let q = db.query(sqlquery, values, parseResult);
            dblog('query', q.sql.slice(0, 500));
        };
        modelobject.getById = function(id, cb) {
            if (db === null) throw new Error('No database connection');
            if (databaseContext.primaryKey === null) throw new Error('Couldnt save `' + name + '`, no primaryKey');
            let obj = {};
            obj[databaseContext.primaryKey] = id;
            modelobject.get(obj, function(err, result) {
                if (err) {
                    cb(err);
                    return;
                }
                if (result.length == 0) {
                    cb(null, null);
                    return;
                }
                cb(null, result.shift());
            });
        };
        modelobject.new = function(cb) {
            cb(null, new modelobject());
        };
        pointer[lastnamespace] = modelobject;
        return {
            primaryKey: function(key) {
                if (Object.keys(model).indexOf(key) == -1) {
                    throw new Error('Primary key `' + key + '` is not found in model `' + name + '`');
                }
                databaseContext.primaryKey = key;
                return this;
            },
            relation: function(modellink, foreignkey) {
                databaseContext.relations.push({
                    model: modellink,
                    key: foreignkey
                });
                return this;
            },
            dbTable: function(tablename) {
                databaseContext.tablename = tablename;
                return this;
            },
            default: function(key, defaultvalue) {
                databaseContext.defaults.push({
                    key: key,
                    defaultvalue: defaultvalue
                });
                return this;
            },
            setConstructor: function(type, func) {
                if (typeof name != 'string' || typeof func != 'function') {
                    return false;
                }
                methods.constructors[name] = func;
                return true;
            },
            createMethod: function(name, func) {
                if (typeof name != 'string' || typeof func != 'function') {
                    return false;
                }
                methods.object[name] = func;
                return true;
            }
        };
    }
};