module.exports = function({ Controller }) {
    const sessionlog = require('debug')('session');
    Controller.createMiddleware('session', function SessionMiddleware(req, res, next) {
        req.session = null;
        if (req.headers.authorization || req.query.authToken) {
            let authtoken = req.query.authToken || req.headers.authorization;
            sessionlog('detected session: ' + authtoken.substr(0, 5) + '..');
            let SessionHelper = require('../Helper/SessionHelper.js');
            SessionHelper.checkKey(req.database, authtoken, (err, result) => {
                if (err == 404) {
                    next();
                    return;
                }
                if (err) {
                    next(err);
                    return;
                }
                req.session = result;
                next();
            });
        } else {
            next();
        }
    }, 'defaults');
};