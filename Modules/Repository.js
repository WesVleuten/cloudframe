/**
 * Repository
 * This code will make repository code avalible to controllers
 * @module Repository
 */

const log = require('debug')('repository');

module.exports = {
    /**
     * Index of all logic
     * @name Repository.repositoryIndex
     */
    repositoryIndex: {},
    
    /**
     * Add new logic to the register
     * @name Repository.register
     * @function
     * @param {string} [name] Name of controller, if not provided. Uses name of function
     * @param {Function} func Function to be run when logic is called
     * @returns {Boolean} Returns if stored sucessfully
     */
    register: function register(name, func) {
        if (typeof name == 'function') {
            func = name;
            name = func.name;
        }
        if (this.repositoryIndex[name] !== undefined) {
            return false;
        }
        this.repositoryIndex[name] = func;
        return true;
    },

    /**
     * Execute logic function
     * @name Repository.execute
     * @function
     * @param {string} name Name of logic
     * @returns {Promise} Returns a promise
     */
    execute: function execute(name) {
        if (this.repositoryIndex[name] === undefined) {
            return new Promise((resove, reject) => {
                reject('Repository not found');
            });
        }
        try {
            const returner = this.repositoryIndex[name].apply({}, Array.prototype.slice.call(arguments, [1]));
            if (Promise.resolve(returner) !== returner) {
                log(`WARN: repository "${name}" did not return a promise, this is bad practice!`);
            }
            return returner;
        } catch (err) {
            return Promise.reject(err);
        }
    }
};
