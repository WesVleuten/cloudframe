/**
 * Logic
 * This code will make logic code avalible to controllers
 * @module Logic
 */

const log = require('debug')('logic');

module.exports = {
    /**
     * Index of all logic
     * @name Logic.logicIndex
     */
    logicIndex: {},
    
    /**
     * Add new logic to the register
     * @name Logic.register
     * @function
     * @param {string} [name] Name of controller, if not provided. Uses name of function
     * @param {Function} func Function to be run when logic is called
     * @returns {Boolean} Returns if stored sucessfully
     */
    register: function register(name, func) {
        if (typeof name == 'function') {
            func = name;
            name = func.name;
        }
        if (this.logicIndex[name] !== undefined) {
            return false;
        }
        this.logicIndex[name] = func;
        return true;
    },

    /**
     * Execute logic function
     * @name Logic.execute
     * @function
     * @param {string} name Name of logic
     * @returns {Promise} Returns a promise
     */
    execute: function execute(name) {
        if (this.logicIndex[name] === undefined) {
            return new Promise((resove, reject) => {
                reject('Logic not found');
            });
        }
        try {
            const returner = this.logicIndex[name].apply({}, Array.prototype.slice.call(arguments, [1]));
            if (Promise.resolve(returner) !== returner) {
                log(`WARN: logic "${name}" did not return a promise, this is bad practice!`);
            }
            return returner;
        } catch (err) {
            return Promise.reject(err);
        }
    }
};
