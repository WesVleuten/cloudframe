module.exports = function({ Controller }) {
    const logFile = require('../Helper/ErrorHelper');
    Controller.createMiddleware('wrapper', function ResponseMiddleware(req, res, next) {
        const logError = function(err) {
            res.status(500).send('Internal Server Error (' + new Date().getTime() + ')');
            logFile('error.log', {
                url: req.originalUrl,
                message: err.message,
                stacktrace: err,
            }).catch(err => {
                console.log(err);
            });
            return;
        };

        res.sendNotFound = data => {
            res.status(404).send(data || 'Not Found');
        };
        res.sendForbidden = data => {
            res.status(403).send(data || 'Forbidden');
        };
        res.sendBadRequest = data => {
            res.status(400).send(data || 'Bad Request');
        };
        res.sendServerError = (err) => {
            logError(err);
        };
        res.sendNoContent = (data) => {
            res.status(204).end(data || 'No Content');
        };
        res.sendWrapped = (err, data) => {
            if (err) {
                logError(err);
                return;
            }
            res.status(200).send(data);
        };

        res.sendPromise = promise => {
            promise.then(result => {
                res.status(200).send(result);
            }).catch(err => {
                logError(err);
            });
        };

        res.sendError = res.sendServerError;
        res.sendErr = res.sendServerError;
        res.sendData = res.sendWrapped;
        next();
    }, 'defaults');
};