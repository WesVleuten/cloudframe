module.exports = {
    checkKey: function(db, key, cb) {
        db.query("SELECT * FROM Session WHERE `key`=?", [ key ], function(err, data) {
            if (err) {
                cb(err);
                return;
            }
            if (data.length == 0) {
                cb(404);
                return;
            }
            if (data.length != 1) {
                cb(new Error('MULTIPLESESSIONFOUND'));
                return;
            }
            cb(null, data.shift());
        });
    }
};